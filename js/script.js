"use strict";

class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  get name() {
    return this._name;
  }

  set name(value) {
    this._name = value;
  }

  get age() {
    return this._age;
  }

  set age(value) {
    this._age = value;
  }

  get salary() {
    return this._salary;
  }

  set salary(value) {
    this._salary = value;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this._lang = lang;
  }

  get lang() {
    return this._lang;
  }

  set lang(value) {
    this._lang = value;
  }

  get salary() {
    return this._salary * 3;
  }

  set salary(value) {
    this._salary = value;
  }
}

const programmer1 = new Programmer("Іван", 30, 1000, ["Python", "JavaScript"]);
const programmer2 = new Programmer("Олена", 25, 1200, ["Java", "C#"]);

console.log(programmer1);
console.log(programmer2);
